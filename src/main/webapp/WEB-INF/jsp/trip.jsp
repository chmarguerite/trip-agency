<!DOCTYPE html>


<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
	<!-- 
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
</head>
<body>

	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">BPI Service Qualification</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="trip">Trip</a></li>
					<li><a href="#about">About</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">



<h2>Trip Choice</h2>
        <form:form method="post" action="calculate" class="form-group">  
            <div class="form-row">
            <div class="form-group col-md-4">
		      <label cssClass="NumeroBon" id="labelNom">Nom du passager</label>
		      <input name="nom" class="form-control" placeholder=""/>
		    </div>
		   
		   <div class="form-row">
             <div class="form-group col-md-4">
		      <label cssClass="destination"  path="labelDestination">Destination</label>
		      <select name="destination" class="form-control" placeholder="" ">
		        <option>London</option>
		         <option>New York</option>
		       <option>Paris</option>
		        <option>Tokyo</option>
		      </select>
		    </div>
		    </div>
		    
		   <div class="form-row">
		   <div class="form-group col-md-12">
			      <label cssClass="Commentaires"  name="labelCommentaires" >Commentaires</label>
			      <textarea name="commentaires" class="form-control" placeholder=""></textarea>
			  </div>
			   </div>
			   <div class="form-row"> 
			   <div class="form-group col-md-12">                     
            	<input type="submit" class="btn btn-primary btn-lg btn-block" value="Calculer le tarif"/>
            	</div>
            	</div>
        </form:form>


	</div>
	
	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>