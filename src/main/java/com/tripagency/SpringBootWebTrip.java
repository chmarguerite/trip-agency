package com.tripagency;

import com.tripagency.data.DestinationData;
import com.tripagency.repositories.DestinationRepository;
import com.tripagency.repositories.TripRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootWebTrip  {

	
    public static void main(String[] args) {
    	
        SpringApplication.run(SpringBootWebTrip.class, args);
    }

    @Bean
	public CommandLineRunner demo(DestinationRepository repository) {
		return (args) -> {
		};
	}
}


