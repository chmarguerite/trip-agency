package com.tripagency.domaine;

public class Trip {
	private Destination destination;
	private double price;

	public Destination getDestination() {return destination;}
	public void setDestination(Destination destination) {this.destination = destination;}
	public double getPrice() {return price;}
	public void setPrice(double price) {this.price = price;}
}
