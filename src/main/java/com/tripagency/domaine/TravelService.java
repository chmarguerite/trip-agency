package com.tripagency.domaine;

import org.springframework.stereotype.Service;

@Service
public class TravelService implements ITravelService {

	private PriceDao tripPriceDao;


	public TravelService(PriceDao tripPriceDao) {
		this.tripPriceDao = tripPriceDao;
	}
	
	public void setTripPriceDao(TripPriceDao tripPriceDao) {
		this.tripPriceDao = tripPriceDao;
	}
	
	public void calculateTripPrice(Trip trip) {
		int distance = tripPriceDao.getDistance(trip);
		int agencyFees = tripPriceDao.getAgencyFees(trip);
		double priceDistance = tripPriceDao.getDistance(trip);

		if(distance <= 500){
			priceDistance = distance *0.5;
		}else if(distance <= 1000){
			priceDistance = distance;

		}else{
			priceDistance = distance * 1.5;
		}
		if(distance >= 800){
			agencyFees = 0;
		}
		trip.setPrice(priceDistance + agencyFees);

	}


}
