package com.tripagency.domaine;

public interface PriceDao {
	public int getDistance(Trip trip);
	public int getAgencyFees(Trip trip);
}
