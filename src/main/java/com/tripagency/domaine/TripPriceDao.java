package com.tripagency.domaine;

import com.tripagency.repositories.DestinationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("Pricer")
public class TripPriceDao implements PriceDao{

    @Autowired
    public DestinationRepository destinationRepository;

	public int getDistance(Trip trip) {
        return destinationRepository.findById(trip.getDestination().getDestinationName()).get().getDistance();
	}

	public int getAgencyFees(Trip trip) {
        return destinationRepository.findById(trip.getDestination().getDestinationName()).get().getAgencyFees();
	}

}
