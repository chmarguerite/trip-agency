package com.tripagency.api;

import com.tripagency.domaine.Destination;
import com.tripagency.domaine.ITravelService;
import com.tripagency.domaine.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/")
public class TravelRestController {

	@Autowired(required=true)
	private ITravelService travelService;

	@RequestMapping(value = "/travelprice/{destination}", method = RequestMethod.GET)
	public Trip getTravelPrice(@PathVariable("destination") String destinationName){
		Trip trip = new  Trip();
		trip.setDestination(new Destination(destinationName));
		
		travelService.calculateTripPrice(trip);
		return trip;
	}

}

