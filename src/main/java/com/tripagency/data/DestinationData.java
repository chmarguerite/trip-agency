package com.tripagency.data;

import javax.persistence.*;

@Entity
@Table(name="destination")
public class DestinationData {
	@Id
	private String destination;
	private int distance;
	private int agencyFees;
	protected DestinationData() {}

	public DestinationData( String destination) {
	    this.destination = destination;
	}
	public DestinationData( String destination, int distance, int agencyFees) {

		this.destination = destination;
		this.distance = distance;
		this.agencyFees = agencyFees;
	}

	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getAgencyFees() { return agencyFees; }
	public void setAgencyFees(int agencyFees) { this.agencyFees = agencyFees; }
	    @Override
	    public String toString() {
	        return String.format(
	                "Destination[destination='%s', distance='%s', agencyFees=%]",
	                destination, distance, agencyFees);
	    }


}
