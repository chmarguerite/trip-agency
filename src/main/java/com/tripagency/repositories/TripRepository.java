package com.tripagency.repositories;

import com.tripagency.data.TripData;
import org.springframework.data.repository.CrudRepository;

public interface TripRepository extends CrudRepository<TripData, String> {

}
