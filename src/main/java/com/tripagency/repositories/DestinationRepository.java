package com.tripagency.repositories;

import com.tripagency.data.DestinationData;
import org.springframework.data.repository.CrudRepository;

public interface DestinationRepository extends CrudRepository<DestinationData, String> {

}
