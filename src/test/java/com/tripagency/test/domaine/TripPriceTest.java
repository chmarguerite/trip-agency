package com.tripagency.test.domaine;

import com.tripagency.domaine.Destination;
import com.tripagency.domaine.TravelService;
import com.tripagency.domaine.Trip;
import com.tripagency.domaine.TripPriceDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TripPriceTest {

    Trip trip;
    TravelService travelPriceComputor;
    TripPriceDao tripPriceDao;

    @Before
    public void setup(){
        tripPriceDao = Mockito.mock(TripPriceDao.class);
        travelPriceComputor = new TravelService(tripPriceDao);
    }


    @Test
    public void testCalculCourteDistance(){
        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip  trip = new Trip();
        trip.setDestination(destination);
        Mockito.when(tripPriceDao.getDistance(trip)).thenReturn(100);
        Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(5);
        travelPriceComputor.calculateTripPrice(trip);
        Assert.assertEquals(55.0,trip.getPrice(), 0.0);

    }

    @Test
    public void testCalculMoyenneDistanceSansPromo(){
        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip  trip = new Trip();
        trip.setDestination(destination);
        Mockito.when(tripPriceDao.getDistance(trip)).thenReturn(600);
        Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(50);
        travelPriceComputor.calculateTripPrice(trip);
        Assert.assertEquals(650.0,trip.getPrice(), 0.0);

    }


    @Test
    public void testCalculMoyenneDistanceAvecPromo(){
        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip  trip = new Trip();
        trip.setDestination(destination);
        Mockito.when(tripPriceDao.getDistance(trip)).thenReturn(800);
        Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(10000000);
        travelPriceComputor.calculateTripPrice(trip);
        Assert.assertEquals(800.0,trip.getPrice(), 0.0);

    }

    @Test
    public void testCalculLongueDistanceAvecPromo(){
        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip  trip = new Trip();
        trip.setDestination(destination);
        Mockito.when(tripPriceDao.getDistance(trip)).thenReturn(1001);
        Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(50);
        travelPriceComputor.calculateTripPrice(trip);
        Assert.assertEquals(1501.5,trip.getPrice(), 0.0);

    }


/*
    @Test
    public void testGetPriceBasicToParis(){

        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip  trip = new Trip();
        trip.setDestination(destination);
        Mockito.when(tripPriceDao.getDistance(trip)).thenReturn(600);
        Mockito.when(tripPriceDao.getAgencyFees(trip)).thenReturn(50);

        //Exécution de la méthode
        travelPriceComputor.calculateTripPrice(trip);

        //Assertions
        Assert.assertEquals(650.0,trip.getPrice(), 0.0);
    }*/



}
