package com.tripagency.test.domaine;


import com.tripagency.domaine.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@RunWith(SpringRunner.class)
@Import(value=AppConfig.class)
@DataJpaTest
@TestPropertySource(locations = "classpath:application.properties")
public class TripPriceIT {


    TravelService travelPriceComputor;
    @Autowired
    TripPriceDao tripPriceDao;


    @Before
    public void setup()  {
        travelPriceComputor = new TravelService(tripPriceDao);
    }


    @Test
    @Sql(scripts ="/data/init1.sql",executionPhase = BEFORE_TEST_METHOD ) // Init
    public void testGetPriceBasicToParis(){

        //Données en entrée: valeur des données
        Destination destination = new Destination("Paris");
        Trip trip  = new Trip();
        trip.setDestination(destination);
        //Exécution de la méthode
        travelPriceComputor.calculateTripPrice(trip);

        //Assertions
        Assert.assertEquals(55.0,trip.getPrice(),0.0);

        //Données en entrée: valeur des données
         destination = new Destination("London");
         trip  = new Trip();
        trip.setDestination(destination);
        //Exécution de la méthode
        travelPriceComputor.calculateTripPrice(trip);

        //Assertions
        Assert.assertEquals(1501.5,trip.getPrice(),0.0);
    }



}
