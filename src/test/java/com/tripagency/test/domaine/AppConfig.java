package com.tripagency.test.domaine;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@ComponentScan(basePackages = {"com.tripagency.domaine"})
@EntityScan("com.tripagency.data")
@EnableJpaRepositories("com.tripagency.repositories")
public class AppConfig {
}
