package com.tripagency.test.domaine;

import com.tripagency.domaine.Calculateur;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculateurTest {
    Calculateur calculateur = new Calculateur();

    @Test
    public void testExemple1(){
        //Données en entrée
        //p=100 & q=50
        // Execution de la fonction à tester
        String result=calculateur.size(100,50);
        //Vérification
        assertThat(result).isEqualTo("P Large");
    }

    @Test
    public void testExemple2(){
        //Données en entrée
        //p=100 & q=50
        // Execution de la fonction à tester
        String result=calculateur.size(50,20);
        //Vérification
        assertThat(result).isEqualTo("");
    }
    @Test
    public void testExemple3(){
        //Données en entrée
        //p=100 & q=50
        // Execution de la fonction à tester
        String result=calculateur.size(80,20);
        //Vérification
        assertThat(result).isEqualTo("P Large");
    }
    @Test
    public void testExemple4(){
        //Données en entrée
        //p=100 & q=50
        // Execution de la fonction à tester
        String result=calculateur.size(50,51);
        //Vérification
        assertThat(result).isEqualTo("Large");
    }
}
