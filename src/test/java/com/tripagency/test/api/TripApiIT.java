package com.tripagency.test.api;

import com.tripagency.domaine.PriceDao;
import com.tripagency.domaine.Trip;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application.properties")
public class TripApiIT {

    @Autowired
    EntityManager entityManager;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    PriceDao tripPriceDao;
    @LocalServerPort //permet d'utiliser le port local du serveur, sinon une erreur "Connection refused"
    private int port;



    @Test
    @Sql(scripts ="/data/init1.sql",executionPhase = BEFORE_TEST_METHOD ) // Initialisation de la db de test
    public void testGetPrice() throws Exception {

        ResponseEntity<Trip> responseEntity =
                restTemplate.getForEntity("/api/v1/travelprice/Paris",  Trip.class);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().getPrice()).isEqualTo(55.0);

    }

}
